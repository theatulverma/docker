**Docker Commands
Some of the most commonly used docker commands are**

- docker images                               #Lists docker images on the host machine.
- docker build                                #Builds image from Dockerfile.
- docker run -it -d <image name>              #Runs a Docker container.
- docker exec -it <container id> bash         #Run a command in a running container.
- docker run -d -> Run container in background and print container ID docker run -p -> Port mapping
- use docker run --help to look into more arguments.
- docker ps                                    #Lists running containers on the host machine.
- docker kill <container id>
- docker stop <container id>                   #Stops running container. 
- docker start                                   #Starts a stopped container.
- docker rm                                       #Removes a stopped container.
- docker rmi <image-id>                                       #Removes an image from the host machine.
- docker pull                                    #Downloads an image from the configured registry.
- docker push                                    #Uploads an image to the configured registry.
- docker network                             #Manage Docker networks such as creating and removing networks, 
- docker login
- docker commit <conatainer id> <username/imagename>
- docker push <username/image name>
- docker build <path to docker file>


